package nyoom.entities;


import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PShape;

public class Unit extends Entity{
	
	private boolean disabled = false; //Can't move or shoot if disabled
	
	
	private float turretCooldown;
	private float turretTimer;
	private int turretDamage;
	
	private int projectileSpawnOffset;
	private float projectileSpawnRand;
	private PShape projectile;

	private Audio shootSound;
	
	private Entity target;
	
	/**
	 * Creates a new Unit
	 * @param l - the location of the unit
	 * @param r - the rotation of the unit
	 * @param m - the visuals of the unit
	 * @param h - the health of the unit
	 * @param s - the shield of the unit
	 * @param projectile - The model of the Unit's projectile.
	 */
	public Unit(Vector3D l, Vector3D r, PShape m, float h, float s, PShape projectile){
		super(l, r, m, h, s);
		
		projectileSpawnOffset = 5;
		projectileSpawnRand = 4;
		this.setProjectile(projectile);
		
		turretCooldown = .4f;
		turretTimer = 0;
		turretDamage = 20; //5
		
		shootSound = new Audio("sound/weapon_mediumlaser_1.wav");
		shootSound.setVolume(-11);
	}
	
	/**
	 * Creates a new Unit
	 * @param l - the location of the unit
	 * @param r - the rotation of the unit
	 * @param u - the unit to use as a template for this unit
	 */
	public Unit(Vector3D l, Vector3D r, Unit u){
		this(l, r, u.getModel(), u.getMaxHealth(), u.getMaxShields(), u.projectile);
		turretCooldown = u.getTurretCooldown();
		turretTimer = turretCooldown;
		turretDamage = u.getTurretDamage(); //5
		target = u.getTarget();
		shootSound = u.getShootSound();
		super.setDeathSound(u.getDeathSound());
	}
	
	/**
	 * draws this unit
	 * @param s - Canvas for the Unit to be drawn on.
	 */
	public void draw(Canvas s){
		if (disabled)
		{
			s.blendMode(s.ADD);
		}
		super.draw(s);
		s.blendMode(s.NORMAL);
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	
	/**
	 * Updates this Unit's timer.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		if (disabled)
			return;
		super.move(fps);
		float frameDelta = (1f / fps);
		
		if(target != null){
			turretTimer -= frameDelta;
		}
	}
	
	/**
	 * Updates this unit's AI, and sets its velocity according to the player's location.
	 * Moves towards the player if within a certain radius.
	 * If the player is within a specified radius, this Unit will open fire on them.
	 * @param c - Canvas which contains the players and all other entities.
	 */
	public void updateAI(Canvas c){
		Player player = c.getPlayer();
		float distanceSqr = getLoc().distanceSquared(player.getLoc());
		Vector3D dir = player.getLoc().difference(getLoc()).normalized();
		Vector3D enemyVelocity = dir.scalarProduct(Canvas.ENEMY_SPEED);

		if (distanceSqr > Canvas.ENEMY_MOVE_RADIUS * Canvas.ENEMY_MOVE_RADIUS)
			setVelocity(new Vector3D());
		else //inside move radius
		{
			setVelocity(enemyVelocity);
			//Too close!
			if (distanceSqr < Canvas.ENEMY_STOP_RADIUS * Canvas.ENEMY_STOP_RADIUS)
				setVelocity(enemyVelocity.scalarProduct(-1));
			if (Canvas.abs(distanceSqr - Canvas.ENEMY_STOP_RADIUS * Canvas.ENEMY_STOP_RADIUS) < Canvas.ENEMY_MOVE_STOP_BUFFER)
				setVelocity(new Vector3D());
		}

		setRot(new Vector3D(0, Canvas.atan2(dir.getX(), dir.getZ()), Canvas.PI));

		if(distanceSqr < Canvas.ENEMY_ATTACK_RADIUS * Canvas.ENEMY_ATTACK_RADIUS)
		{
			addProjectile(c);
		}

	}
	
	
	/**
	 * Creates a new projectile and adds it to the level
	 * @param drawer - canvas to add projectile to
	 */
	public void addProjectile(Canvas drawer){
		if(target == null || turretTimer > 0 || disabled)
			return;
		if(!drawer.getEntities().contains(target) && !(target instanceof Player)){
			target = null;
			return;
		}
		shootSound.reset();
		shootSound.play();
		
		Vector3D loc = new Vector3D(getLoc());
		loc.setY(loc.getY() - projectileSpawnOffset);

		Vector3D start = new Vector3D((float)(loc.getX() + (Math.random() - 0.5f) * projectileSpawnRand),
				projectileSpawnOffset,
				(float)(loc.getZ() + (Math.random() - 0.5f) * projectileSpawnRand));
		Vector3D dir = target.getLoc().difference(start).normalized();
		Projectile p = new Projectile(start, dir, projectile, turretDamage, 200, target);
		drawer.getEntities().add(p);
		
		turretTimer = turretCooldown;
	}

	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}

	public Audio getShootSound() {
		return shootSound;
	}

	public void setShootSound(Audio shootSound) {
		this.shootSound = shootSound;
	}

	public float getTurretCooldown() {
		return turretCooldown;
	}

	public void setTurretCooldown(float turretCooldown) {
		this.turretCooldown = turretCooldown;
	}

	public int getTurretDamage() {
		return turretDamage;
	}

	public void setTurretDamage(int turretDamage) {
		this.turretDamage = turretDamage;
	}

	protected void setProjectileSpawnRand(float projectileSpawnRand) {
		this.projectileSpawnRand = projectileSpawnRand;
	}

	public PShape getProjectile() {
		return projectile;
	}

	protected void setProjectile(PShape projectile) {
		this.projectile = projectile;
	}
	
	

}
