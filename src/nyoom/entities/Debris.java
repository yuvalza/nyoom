package nyoom.entities;

import nyoom.graphics.Vector3D;
import processing.core.PShape;

public class Debris extends Entity {
	private Vector3D rotSpeed;
	
	/**
	 * Creates a new structure
	 * @param l - location of this structure
	 * @param r - rotation of this structure
	 * @param m - visuals of this structure
	 * @param rotSpeed - speed at which this asteroid rotates.
	 */
	public Debris(Vector3D l, Vector3D r, PShape m, Vector3D rotSpeed){
		super(l, r, m, 1, 1);
		this.rotSpeed = rotSpeed;
	}
	
	/**
	 * Make a copy of another Debris in this new Debris
	 * @param l - location
	 * @param r - rotation
	 * @param other - Debris template
	 */
	public Debris(Vector3D l, Vector3D r, Debris other){
		this(l, r, other.getModel(), other.rotSpeed);
	}
	
	/**
	 * Rotates this asteroid.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		float frameDelta = 1f/fps;
		Vector3D oldRot = super.getRot();
		super.setRot(new Vector3D(oldRot.getX() + frameDelta * rotSpeed.getX(),
				oldRot.getY() + frameDelta * rotSpeed.getY(),
				oldRot.getZ() + frameDelta * rotSpeed.getZ()));
	}
}
