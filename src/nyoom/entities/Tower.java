package nyoom.entities;


import java.awt.Color;

import nyoom.abilities.Beam;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PShape;

public class Tower extends Entity{

	private float turretCooldown;
	private float turretTimer;
	private int turretDamage;

	private int beamSpawnOffset;

	private Audio shootSound;
	private Audio impactSound;

	private Entity target;

	/***
	 * Creates a new tower
	 * @param l - location of this tower
	 * @param r - rotation of this tower
	 * @param m - visuals of this tower
	 * @param h - health of this tower
	 * @param s - shield of this tower
	 */
	public Tower(Vector3D l, Vector3D r, PShape m, float h, float s){
		super(l, r, m, h, s);

		turretCooldown = .5f;
		turretTimer = 0;
		turretDamage = 50; //50

		shootSound = new Audio("sound/weapon_tower_3.wav");
		shootSound.setVolume(-5f);
		beamSpawnOffset = 3;
		impactSound = new Audio("sound/damage_shield_1_long.wav");
		impactSound.setVolume(-3f);

		super.setDeathSound(new Audio("sound/death_nebulonB_2.wav"));
	}

	/**
	 * Creates a new tower
	 * @param l - location of this tower
	 * @param r - rotation of this tower
	 * @param t - template for this tower
	 */
	public Tower(Vector3D l, Vector3D r, Tower t){
		this(l, r, t.getModel(), t.getMaxHealth(), t.getMaxShields());
		turretCooldown = t.getTurretCooldown();
		turretTimer = turretCooldown;
		turretDamage = t.getTurretDamage(); //5
		target = t.getTarget();
		shootSound = t.getShootSound();
	}

	/***
	 * Creates a new beam and adds it to the level
	 * @param drawer - canvas to add beam to
	 */
	public void addBeam(Canvas drawer){
		if(target == null || turretTimer > 0)
			return;
		if(!drawer.getEntities().contains(target) && !(target instanceof Player)){
			target = null;
			return;
		}
		shootSound.reset();
		shootSound.play();
		impactSound.reset();
		impactSound.play();

		Vector3D loc = new Vector3D(getLoc());
		loc.setY(loc.getY() - beamSpawnOffset);

		Beam beam = new Beam(loc, target, drawer.getEntities(), 0.3f, turretDamage, new Color(255, 255, 255), new Color(255, 200, 50), "sound/ambient_aoe_1.wav");
		beam.setWavyFactor(1);
		beam.setWaveRate(1);
		beam.setRandomAmount(.2f);
		beam.setInnerThickness(2);
		beam.setOuterThickness(3);
		beam.setWaveAmplitude(0);
		beam.getAmbient().setVolume(-40f);
		drawer.getEntities().add(beam);

		turretTimer = turretCooldown;
	}

	public void updateAI(){

	}

	/**
	 * Updates this turret's timer.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		super.move(fps);
		float frameDelta = (1f / fps);

		if(target != null){
			turretTimer -= frameDelta;
		}
	}


	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}

	public Audio getShootSound() {
		return shootSound;
	}

	public void setShootSound(Audio shootSound) {
		this.shootSound = shootSound;
	}

	public float getTurretCooldown() {
		return turretCooldown;
	}

	public void setTurretCooldown(float turretCooldown) {
		this.turretCooldown = turretCooldown;
	}

	public int getTurretDamage() {
		return turretDamage;
	}

	public void setTurretDamage(int turretDamage) {
		this.turretDamage = turretDamage;
	}



}
