package nyoom.entities;

import java.awt.Color;

import nyoom.abilities.AoE;
import nyoom.abilities.Beam;
import nyoom.entities.Entity;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PShape;

public class Player extends Unit {
	private float[] abilityCooldowns; //Starting cooldowns
	private float[] abilityTimers; //Current cooldown timers


	private static final int ABILITY_1_DUR = 8;
	private static final int ABILITY_1_DPS = 25;


	private static final int ABILITY_2_COST = 100;
	private static final int ABILITY_2_DAMAGE = 150;


	private static final float ABILITY_3_DUR = 5;
	private float ability3DurTimer;
	private Unit ability3Target; //Unit to keep disabled with ability 3


	public final static float SHIELD_REGEN_RATE = 50;
	private static final float SHIELD_COOLDOWN = 3;
	private float shieldTimer;


	private Audio ability1sound;
	private Audio ability2sound;
	private Audio ability3sound;

	private Audio lowHealthSound;
	private Audio abilityFailSound;



	/**
	 * Creates a new player object
	 * @param l - location
	 * @param r - rotation
	 * @param m - model
	 * @param projectile - Model for the laser shot by the ship.
	 */
	public Player(Vector3D l, Vector3D r, PShape m, PShape projectile)
	{
		this(l, r, m, 1, 1, projectile);
	}

	/**
	 * Creates a new player object
	 * @param l - location
	 * @param r - rotation
	 * @param m - model
	 * @param h - max health
	 * @param s - max shields
	 * @param projectile - Model for the laser shot by the ship.
	 */
	public Player(Vector3D l, Vector3D r, PShape m, int h, int s, PShape projectile)
	{
		super(l, r, m, h, s, projectile);
		abilityCooldowns = new float[3];
		abilityCooldowns[0] = 28; //20
		abilityCooldowns[1] = 18; //12
		abilityCooldowns[2] = 21; //14

		super.setDeathSound(new Audio("sound/death_isd1_1.wav"));

		shieldTimer = 3;
		abilityTimers = new float[3];

		ability3DurTimer = ABILITY_3_DUR;

		ability1sound = new Audio("sound/ability_aoe_1.wav");
		ability1sound.setVolume(-2);
		ability2sound = new Audio("sound/ability_superlaser_1.wav");
		ability2sound.setVolume(-4);
		ability3sound = new Audio("sound/ability_tractor_1.wav");
		ability3sound.setVolume(-4);

		lowHealthSound = new Audio("sound/ambient_isd1siren_2.wav");
		lowHealthSound.setVolume(-7);
		abilityFailSound = new Audio("sound/ability_fail_1.wav");
		abilityFailSound.setVolume(-5);

		super.setProjectileSpawnRand(20f);
	}


	public Entity getTarget(){
		return super.getTarget();
	}


	public void setTarget(Entity e){
		super.setTarget(e);
	}

	/**
	 * Proton Field
	 * Z: Deploys a hex-shaped AoE centered around the player that damages nearby enemies - once deployed, stays for X seconds then goes on cooldown for X seconds.
	 * @param drawer - canvas to add the AoE entity to
	 */
	public void ability1(Canvas drawer)
	{
		if (abilityTimers[0] > 0)
			return;

		AoE ability = new AoE(new Vector3D(getLoc()), getRot(), drawer.getEntities(), ABILITY_1_DUR, ABILITY_1_DPS);
		ability.getAmbient().setVolume(-3f);
		drawer.getEntities().add(ability);

		ability1sound.reset();
		ability1sound.play();

		abilityTimers[0] = abilityCooldowns[0];
	}

	/**
	 * Superlaser
	 * X: Shoots high powered laser at a single target, instantly consumes some of your shields - can be fired again if you have sufficient shields and the X second cooldown is over.
	 * @param drawer - canvas to add the laser entity to
	 */
	public void ability2(Canvas drawer)
	{
		if (abilityTimers[1] > 0)
			return;

		if (super.getTarget() == null)
		{
			abilityFailSound.reset();
			abilityFailSound.play();
			return;
		}

		if (getShields() - ABILITY_2_COST < 0)
		{
			abilityFailSound.reset();
			abilityFailSound.play();
			return;
		}

		setShields(getShields() - ABILITY_2_COST);

		Beam ability = new Beam(getLoc(), super.getTarget(), drawer.getEntities(), 1.0f, ABILITY_2_DAMAGE, new Color(255, 255, 255), new Color(200, 255, 0), "sound/ambient_aoe_1.wav");
		ability.setWavyFactor(-20);
		ability.setWaveRate(3);
		ability.setRandomAmount(2);
		ability.setInnerThickness(2);
		ability.setOuterThickness(8);
		ability.setWaveAmplitude(1);
		ability.setTaperFactor(.1f);
		ability.getAmbient().setVolume(0f);
		drawer.getEntities().add(ability);

		ability2sound.reset();
		ability2sound.play();

		shieldTimer = SHIELD_COOLDOWN;
		abilityTimers[1] = abilityCooldowns[1];
	}

	/**
	 * Deals damage to this Player. Updates shields and health pools accordingly.
	 * @param damage - Damage to be taken.
	 */
	public void takeHit(float damage){
		super.takeHit(damage);
		shieldTimer = SHIELD_COOLDOWN;

		if ((super.getHealth() / super.getMaxHealth()) < 0.2f)
			lowHealthSound.play();
		if (!super.isAlive())
			lowHealthSound.reset();
	}

	/**
	 * Tractor Beam
	 * C: Freezes the selected enemy in place, disabling any attack possible for that unit.
	 * @param drawer - Canvas for the ability to be drawn on.
	 */
	public void ability3(Canvas drawer)
	{
		if (abilityTimers[2] > 0)
			return;

		//Can't disable asteroids or turrets
		if (!(super.getTarget() instanceof Unit))
		{
			abilityFailSound.reset();
			abilityFailSound.play();
			return;
		}

		ability3Target = ((Unit)super.getTarget());
		ability3Target.setDisabled(true);

		Beam ability = new Beam(getLoc(), ability3Target, drawer.getEntities(), ABILITY_3_DUR, 0, new Color(10, 150, 230, 127), new Color(0, 200, 255, 127), "sound/ambient_tractor_1.wav");
		ability.setWavyFactor(-20);
		ability.setWaveRate(3);
		ability.setRandomAmount(0);
		ability.setInnerThickness(12);
		ability.setOuterThickness(16);
		ability.setWaveAmplitude(8);
		ability.setTaperFactor(0.8f);
		ability.getAmbient().setVolume(-6f);
		drawer.getEntities().add(ability);

		ability3sound.reset();
		ability3sound.play();

		abilityTimers[2] = abilityCooldowns[2];
		ability3DurTimer = ABILITY_3_DUR;
	}

	/**
	 * Updates all timers, and moves the player in its current direction.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps) {
		super.move(fps);
		float frameDelta = (1f / fps);
		if(shieldTimer > 0){
			shieldTimer -= frameDelta;
		} else if(getShields() < getMaxShields()){
			if(getShields() + SHIELD_REGEN_RATE * frameDelta > getMaxShields())
				setShields(getMaxShields());
			else
				setShields(getShields() + SHIELD_REGEN_RATE * frameDelta);
		}

		for (int i = 0; i < abilityTimers.length; i++){
			if(abilityTimers[i] > 0){
				if(abilityTimers[i] - frameDelta < 0)
					abilityTimers[i] = 0;
				else
					abilityTimers[i] -= frameDelta;
			}
		}
		if (ability3DurTimer <= 0)
		{
			ability3DurTimer = 0;
			if (ability3Target != null) {
				ability3Target.setDisabled(false);
				ability3Target = null;
			}
		}
		else
			ability3DurTimer -= frameDelta;
	}

	public float getTimer(int index){
		return abilityTimers[index];
	}

	public float getCooldown(int index){
		return abilityCooldowns[index];
	}
}
