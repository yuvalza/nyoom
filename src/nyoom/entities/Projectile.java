package nyoom.entities;

import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PShape;

public class Projectile extends Entity {
	private int damage;
	private float speed;
	private Vector3D dir;
	private Vector3D targetLoc;
	private Entity target;
	private static final int SQUARED_COLLISION_TOLERANCE = 169;
	
	/**
	 * Creates a new projectile
	 * @param origin - where to create it
	 * @param dir - direction of projectile
	 * @param model - the visuals of the projectile
	 * @param damage - the damage of the projectile
	 * @param speed - how fast the projectile travels
	 * @param target - Target for the projectile.
	 */
	public Projectile(Vector3D origin, Vector3D dir, PShape model, int damage, float speed, Entity target) {
		super(new Vector3D(origin), new Vector3D(0, (float)(Math.atan2(dir.getX(), dir.getZ())), 0), model);
		setVelocity(new Vector3D(speed * dir.getX(), speed * dir.getY(), speed * dir.getZ()));
		this.damage = damage;
		this.speed = speed;
		targetLoc = new Vector3D(target.getLoc());
		this.target = target;
		Audio impact = new Audio("sound/damage_shield_1.wav");
		impact.setVolume(-7f);
		super.setDeathSound(impact);
	}

	/**
	 * Creates a new projectile
	 * @param origin - where to create it
	 * @param template - the projectile it is copied from
	 */
	public Projectile(Vector3D origin, Projectile template) {
		super(origin, new Vector3D(), template.getModel());
		this.setDamage(template.getDamage());
		this.setSpeed(template.getSpeed());
	}

	/**
	 * Creates a new projectile
	 * @param model - the visuals of the projectile
	 * @param damage - the damage of the projectile 
	 * @param speed - the speed of the projectile
	 */
	public Projectile(PShape model, int damage, float speed) {
		super(new Vector3D(), new Vector3D(), model);
		this.setDamage(damage);
		this.setSpeed(speed);
	}


	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
		this.setVelocity(new Vector3D(speed * dir.getX(), speed * dir.getY(), speed * dir.getZ()));
	}
	
	/**
	 * Draws this projectile, and checks for collision with its target.
	 * @param s - The canvas to be drawn on.
	 */
	public void draw(Canvas s){
		if(getLoc().distanceSquared(targetLoc) < SQUARED_COLLISION_TOLERANCE){
			target.takeHit(damage);
			die();
		}
		s.blendMode(s.ADD);
		super.draw(s);
		s.blendMode(s.NORMAL);
	}
}
