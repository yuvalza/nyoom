package nyoom.entities;

import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PApplet;
import processing.core.PShape;

public class Entity {

	private Vector3D loc;
	private Vector3D rot;

	private float shields;
	private float health;
	private float maxHealth;
	private float maxShields;
	private PShape model;
	
	private boolean isAlive;

	private Vector3D velocity;
	
	private Audio deathSound;
	
	/**
	 * creates a new default entity with 1 health and shields, at a default position with no rotation
	 * it is alive and is not moving, it also has no physical representation
	 */
	public Entity(){
		this(new Vector3D(), new Vector3D(), new PShape());
	}
	
	/**
	 * creates a new entity with 1 health and shields, at a specified location with specified rotation and a PShape model, has zero velocity
	 * @param l - location of entity
	 * @param r - rotation of entity
	 * @param m - PShape model of the entity
	 */
	public Entity(Vector3D l, Vector3D r,  PShape m){
		this (l, r, m, 1, 1);
	}
	
	/**
	 * Creates a new Entity with a specified shield ad health. It is at a specified location and a specified rotation 
	 * it has a PShape model of itself, and has zero velocity
	 * @param l - location of Entity
	 * @param r - rotation of Entity
	 * @param m - PShape model of the Entity
	 * @param h - starting health of the Entity
	 * @param s - starting shield of the Entity
	 */
	public Entity(Vector3D l, Vector3D r, PShape m, float h, float s){
		setLoc(l);
		setModel(m);
		setRot(r);
		shields = s;
		health = h;
		maxShields = s;
		maxHealth = h;
		velocity = new Vector3D(0, 0, 0);
		isAlive = true; 
		deathSound = new Audio("sound/death_nebulonb_1.wav");
	}
	
	/**
	 * Make a copy of another Entity in this new Entity
	 * @param other - The other Entity this Entity is making a copy of
	 */
	public Entity(Entity other){
		this(other.loc, other.rot, other.model, other.maxHealth, other.maxShields);
		shields = other.shields;
		health = other.health;
		velocity = other.velocity;
	}
	
	/***
	 * Make a copy of another Entity in this new Entity
	 * @param l - location
	 * @param r - rotation
	 * @param other - entity template
	 */
	public Entity(Vector3D l, Vector3D r, Entity other){
		this(l, r, other.model, other.maxHealth, other.maxShields);
		shields = other.shields;
		health = other.health;
		velocity = other.velocity;
	}
	
	/**
	 * draws the Entity onto the screen
	 * @param s - the surface on which it draws
	 */
	public void draw(Canvas s){
		if(!isAlive) {
			return;
		} 
		s.pushMatrix();
		s.translate(loc.getX(), loc.getY(), loc.getZ());
		s.rotateX(rot.getX());
		s.rotateY(rot.getY());
		s.rotateZ(rot.getZ());
		s.shape(model);
		s.popMatrix();
	}


	/**
	 * takes damage by the specified amount, lowers shields first, health otherwise
	 * @param damage - the amount of damage the entity takes
	 */
	public void takeHit(float damage) {
		if(shields - damage <= 0){
			shields = 0; 
			health -= damage - shields;
		} else {
			shields -= damage;
		} 
		
		if(health <= 0){
			health = 0;
			die();
		}

	}

	/**
	 * makes this Entity dead, so it is not drawn anymore
	 */
	public void die(){
		if (!isAlive)
			return;
		deathSound.reset();
		deathSound.play();
		isAlive = false;
	}

	public Vector3D getRot() {
		return rot;
	}

	public void setRot(Vector3D rot) {
		this.rot = rot;
	}

	public Vector3D getLoc() {
		return loc;
	}

	public void setLoc(Vector3D loc) {
		this.loc = loc;
	}

	public PShape getModel() {
		return model;
	}

	public void setModel(PShape model) {
		this.model = model;
	}
	
	public boolean isAlive(){
		return isAlive;
	}
	
	
	/**
	 * Moves Entity by its velocity. Takes in a framerate to allow for FPS independence
	 * @param fps - the current framerate of the game
	 */
	public void move(float fps){
		float deltaFrame = 1 / fps;
		loc.setX(loc.getX() + velocity.getX() * deltaFrame);
		loc.setY(loc.getY() + velocity.getY() * deltaFrame);
		loc.setZ(loc.getZ() + velocity.getZ() * deltaFrame);
	}
	
	public Vector3D getVelocity() {
		return velocity;
	}
	public void setVelocity(Vector3D velocity) {
		this.velocity = velocity;
	}
	public float getMaxShields() {
		return maxShields;
	}
	public void setMaxShields(float maxShields) {
		this.maxShields = maxShields;
	}
	public float getMaxHealth() {
		return maxHealth;
	}
	public void setMaxHealth(float maxHealth) {
		this.maxHealth = maxHealth;
	}
	public float getHealth() {
		return health;
	}

	public float getShields(){
		return shields;
	}
	public void setHealth(float newH) {
		health = newH;
	}

	public void setShields(float newS){
		shields = newS;
	}
	
	public boolean isColliding(Entity e) {
		
		return false;
	}

	public Audio getDeathSound() {
		return deathSound;
	}

	public void setDeathSound(Audio deathSound) {
		this.deathSound = deathSound;
	}

}
