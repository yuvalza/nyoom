package nyoom.graphics;

import java.util.ArrayList;

import nyoom.entities.Debris;
import nyoom.entities.Entity;
import nyoom.entities.Tower;
import nyoom.entities.Unit;
import nyoom.objectives.DestroyEntity;
import nyoom.objectives.Hint;
import nyoom.objectives.Objective;
import nyoom.objectives.ReachLocation;
import processing.core.PShape;

public class Level {
	private ArrayList<Entity> ents;
	private ArrayList<Objective> goals;
	private Vector3D defaultRot;
	private Canvas c;
	private PShape hintModel;
	
	/**
	 * Constructs a new Level object, to be drawn on Canvas c.
	 * @param c - The canvas to draw the level on.
	 * @param hintModel - A model, used to hint the player where to go.
	 */
	public Level(Canvas c, PShape hintModel){
		ents = c.getEntities();
		goals = c.getObjectives();
		defaultRot = new Vector3D();
		this.hintModel = hintModel;
		this.c = c;
	}
	
	/**
	 * Inits the first enemy group to be destroyed.
	 * @param enemy - Model of the enemys in the enemy group.
	 */
	public void firstEnemyGroup(Entity enemy){
		ArrayList<Entity> targs = new ArrayList<Entity>();
		targs.add(new Unit(new Vector3D(340, 0, 273), defaultRot, (Unit)enemy));
		targs.add(new Unit(new Vector3D(386, 0, 312), defaultRot, (Unit)enemy));
		targs.add(new Unit(new Vector3D(300, 0, 300), defaultRot, (Unit)enemy));
		targs.add(new Unit(new Vector3D(333, 0, 343), defaultRot, (Unit)enemy));
		DestroyEntity goal = new DestroyEntity("Destroy the 4 Nebulon B cruisers of the Bleeding Edge squadron.", "Good work! Rebel scum quashed!", targs, 85);
		for (Entity neb : targs)
			ents.add(new Hint(neb, new Vector3D(), hintModel, goal));
		ents.addAll(targs);
		goals.add(goal);
	}
	
	/**
	 * Inits the second enemy group to be destroyed.
	 * @param enemy - Model of the enemys in the enemy group.
	 */
	public void secondEnemyGroup(Entity enemy){
		ArrayList<Entity> targs = new ArrayList<Entity>();
		targs.add(new Unit(new Vector3D(200, 0, 820), defaultRot, (Unit)enemy));
		targs.add(new Unit(new Vector3D(130, 0, 730), defaultRot, (Unit)enemy));
		DestroyEntity goal = new DestroyEntity("Eliminate the 2 Nebulon B cruisers defending the stolen-[END TRANSMISSION]", "Good work! Rebel scum quashed!", targs, 145);
		for (Entity neb : targs)
			ents.add(new Hint(neb, new Vector3D(), hintModel, goal));
		ents.addAll(targs);
		goals.add(goal);
	}

	/**
	 * Inits the last fight, against the level boss.
	 * @param enemy - Model of the boss.
	 */
	public void bossGroup(Entity enemy){
		ArrayList<Entity> targs = new ArrayList<Entity>();
		targs.add(new Unit(new Vector3D(-198, 0, 910), defaultRot, (Unit)enemy));
		DestroyEntity goal = new DestroyEntity("Reclaim the Emperor's property!", "lol you will never see this message", targs, 240);
		for (Entity isd : targs)
			ents.add(new Hint(isd, new Vector3D(), hintModel, goal));
		ents.addAll(targs);
		goals.add(goal);
	}

	/**
	 * Inits the first reach location objective.
	 */
	public void firstLocation(){
		Vector3D goalLoc = new Vector3D(0, 0, 175);
		Objective goal = new ReachLocation("Reach the forward position above the asteroid.", "Position secured.", goalLoc, 15);
		Hint hint = new Hint(goalLoc, new Vector3D(), hintModel, goal);
		ents.add(hint);
		goals.add(goal);
	}
	
	/**
	 * Inits the second reach location objective.
	 */
	public void secondLocation(){
		Vector3D goalLoc = new Vector3D(325, 0, 622);
		Objective goal = new ReachLocation("Secure the asteroid cluster ahead.", "Excellent work.", goalLoc, 105);
		Hint hint = new Hint(goalLoc, new Vector3D(), hintModel, goal);
		ents.add(hint);
		goals.add(goal);
	}

	/**
	 * Inits the third reach location objective.
	 */
	public void thirdLocation(){
		Vector3D goalLoc = new Vector3D(0, 0, 826);
		Objective goal = new ReachLocation("-we think it's located ahead of you... comms being jammed-[CONNECTION LOST]", "-regaining connection-", goalLoc, 175);
		Hint hint = new Hint(goalLoc, new Vector3D(), hintModel, goal);
		ents.add(hint);
		goals.add(goal);
	}

	/**
	 * Adds some enemy units to the level at predefined locations.
	 * @param enemy - Template entity for enemy units.
	 */
	public void assortedEnemies(Unit enemy){
		ArrayList<Unit> targs = new ArrayList<Unit>();
		targs.add(new Unit(new Vector3D(110, 0, 264), defaultRot, enemy));
		targs.add(new Unit(new Vector3D(415, 0, 691), defaultRot, enemy));
		targs.add(new Unit(new Vector3D(370, 0, 730), defaultRot, enemy));
		targs.add(new Unit(new Vector3D(-241, 0, 840), defaultRot, enemy));
		targs.add(new Unit(new Vector3D(-50, 0, 488), defaultRot, enemy));
		ents.addAll(targs);
	}

	/**
	 * Adds some towers to the level at predefined locations.
	 * @param tower - Template entity for the towers.
	 */
	public void assortedTowers(Tower tower){
		ArrayList<Tower> targs = new ArrayList<Tower>();
		targs.add(new Tower(new Vector3D(340, 0, 530), defaultRot, tower));
		targs.add(new Tower(new Vector3D(172, 0, 761), defaultRot, tower));
		ents.addAll(targs);
	}

	/**
	 * Generates a cluster of towers which attack the user.
	 * @param tower - Template entity for the towers.
	 */
	public void obstacleTowers(Tower tower){
		ArrayList<Tower> targs = new ArrayList<Tower>();
		targs.add(new Tower(new Vector3D(0, 0, 617), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-5, 0, 540), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-60, 0, 581), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-106, 0, 531), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-166, 0, 568), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-189, 0, 520), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-240, 0, 552), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-300, 0, 500), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-350, 0, 550), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-400, 0, 500), defaultRot, tower));
		targs.add(new Tower(new Vector3D(-460, 0, 544), defaultRot, tower));
		ents.addAll(targs);
	}

	/**
	 * Adds many debris for level decoration.
	 * @param big - Big asteroid entity.
	 * @param medium - Medium asteroid Entity.
	 * @param small - Small asteroid entity.
	 */
	public void decoration(Entity big, Entity medium, Entity small){
		ArrayList<Debris> asteroids = new ArrayList<Debris>();
		float bigOffset = 80;
		asteroids.add(new Debris(new Vector3D(0, bigOffset, 175), randomRotation(), (Debris)big));
		asteroids.add(new Debris(new Vector3D(397, bigOffset, 513), randomRotation(), (Debris)big));
		asteroids.add(new Debris(new Vector3D(188, bigOffset, 606), randomRotation(), (Debris)big));
		asteroids.add(new Debris(new Vector3D(108, bigOffset, 643), randomRotation(), (Debris)big));
		asteroids.add(new Debris(new Vector3D(259, bigOffset, 855), randomRotation(), (Debris)big));
		asteroids.add(new Debris(new Vector3D(-301, bigOffset, 590), randomRotation(), (Debris)big));
		float mediumOffset = -20;
		asteroids.add(new Debris(new Vector3D(395, mediumOffset, 207), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(243, mediumOffset, 260), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(220, mediumOffset, 276), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(-47, mediumOffset, 250), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(262, mediumOffset, 352), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(220, mediumOffset, 568), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(452, mediumOffset, 600), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(248, mediumOffset, 805), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(319, mediumOffset, 837), randomRotation(), (Debris)medium));
		asteroids.add(new Debris(new Vector3D(236, mediumOffset, 683), randomRotation(), (Debris)medium));
		ents.addAll(asteroids);
		float smallOffset = 15;
		scatterAsteroids(new Vector3D(0, smallOffset, 175), (Debris)small, 7, 30.0f);
		scatterAsteroids(new Vector3D(340, smallOffset, 530), (Debris)small, 7, 30.0f);
	}
	
	/**
	 * Adds a given amount of asteroids around a certain location.
	 * @param where - The location for the asteroids to be scattered about.
	 * @param what - The Asteroid entity.
	 * @param number - Number of asteroids to be placed.
	 * @param random - Size of the spread of the asteroids' locations.
	 */
	public void scatterAsteroids(Vector3D where, Debris what, int number, float random){
		for (int i = 0; i < number; i++)
		{
			Entity obj = new Debris(new Vector3D(where.getX() + random * c.random(1) * 2 - 1,
					where.getY(),
					where.getZ() + random * c.random(1) * 2 - 1), randomRotation(), what);
			ents.add(obj);
		}
	}

	/**
	 * Generates a random rotation vector.
	 * @return A Vector3D with random values between 0 and 2PI.
	 */
	public Vector3D randomRotation(){
		return new Vector3D(c.random(1) * 2 * c.PI, c.random(1) * 2 * c.PI, c.random(1) * 2 * c.PI);
	}

	
}
