package nyoom.graphics;
import java.util.ArrayList;
import com.sun.glass.events.KeyEvent;
import nyoom.abilities.Ability;
import nyoom.entities.Debris;
import nyoom.entities.Entity;
import nyoom.entities.Player;
import nyoom.entities.Tower;
import nyoom.entities.Unit;
import nyoom.menus.InstructionMenu;
import nyoom.menus.OnDeathMenu;
import nyoom.menus.StartMenu;
import nyoom.menus.VictoryMenu;
import nyoom.objectives.Objective;
import nyoom.sound.Audio;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PShape;


public class Canvas extends PApplet{
	private Player player;
	private Vector3D cameraPos;
	private Vector3D cameraRot;

	private boolean pW;
	private boolean pA;
	private boolean pS;
	private boolean pD;

	private ArrayList<Entity> ents;


	private int state;
	public static final int IN_GAME = 0;
	public static final int GAME_OVER = 1;
	public static final int IN_START_MENU = 2;
	public static final int INSTRUCTIONS = 3;
	public static final int VICTORY = 4;


	private PImage background;
	private HUD hud;

	private StartMenu startMenu;
	private OnDeathMenu onDeath;
	private VictoryMenu victoryMenu;
	private InstructionMenu instructions;


	private Audio gameMusic;
	private Audio startMenuMusic;
	private Audio deathMenuMusic;
	private Audio gameStartSound;
	private Audio uiErrorSound;
	private Audio gameOverSound;



	private long initTime;
	private float elapsedTime;

	private ArrayList<Objective> goals;
	private static final int GOAL_CHECK_FREQUENCY = 14; //How many frames go by until we check goals conditions again?
	private int goalCheckCounter = 0;


	public static final int ENEMY_ATTACK_RADIUS = 150;
	public static final int TOWER_ATTACK_RADIUS = 200;
	public static final int ENEMY_MOVE_RADIUS = 200;
	//private static final int BOSS_MOVE_RADIUS = 250;
	//private static final int BOSS_ATTACK_RADIUS = 200;
	public static final int ENEMY_STOP_RADIUS = 64;
	public static final int ENEMY_MOVE_STOP_BUFFER = 40;
	public static final float ENEMY_SPEED = 24;
	public static final float ENEMY_HEALTH = 500; //500
	public static final float BOSS_HEALTH = 2000; //500
	public static final float TOWER_HEALTH = 700; //900

	public static final int PLAYER_ATTACK_RADIUS = 200;
	public static final float PLAYER_SPEED_FORWARD = 21; //21f
	public static final float PLAYER_SPEED_BACKWARD = 14f;
	public static final float PLAYER_TURN_SPEED = 0.2f; //0.2f
	public static final int PLAYER_MAX_SHIELDS_E = 2000;
	public static final int PLAYER_MAX_HEALTH_E = 2000;
	public static final int PLAYER_MAX_SHIELDS_M = 1000;
	public static final int PLAYER_MAX_HEALTH_M = 1000;
	public static final int PLAYER_MAX_SHIELDS_H = 900;
	public static final int PLAYER_MAX_HEALTH_H = 100;
	public int playerMaxShields;
	public int playerMaxHealth;

	private int difficulty;
	public static final int EASY = 0;
	public static final int MEDIUM = 1;
	public static final int HARD = 2;



	//private Color healthColor = 

	/**
	 * Initializes a Canvas, starting an OpenGL window.
	 */
	public Canvas(){

		pW = false;
		pA = false;
		pS = false;
		pD = false;


		cameraPos = new Vector3D(0, 0, 0);
		cameraRot = new Vector3D(0, 0, 0);


		startMenuMusic = new Audio("sound/music_title_1.wav");
		startMenuMusic.setVolume(-2);
		gameMusic = new Audio("sound/music_bkg_1.wav");
		gameMusic.setVolume(-7);
		deathMenuMusic = new Audio("sound/music_death_1.wav");
		deathMenuMusic.setVolume(0);

		gameStartSound = new Audio("sound/hyperspace_enter_1.wav");
		gameStartSound.setVolume(-3);
		gameOverSound = new Audio("sound/hyperspace_enter_1.wav");
		gameOverSound.setVolume(-3);
		uiErrorSound = new Audio("sound/ui_error_3.wav");
		uiErrorSound.setVolume(-14);


		state = 2;

		runSketch();
	}


	/**
	 * Draws HUD, adds lighting and camera manipulations, and draws all Entities in existence.
	 * Called at a target of 60 times a second.
	 */
	public void draw(){
		clear();
		background(0);
		switch(state){
		case IN_START_MENU:
			camera();
			startMenu.draw();
			break;
		case INSTRUCTIONS:
			camera();
			instructions.draw();
			break;
		case IN_GAME:
			hud.draw();
			setupCamera();

			elapsedTime = (float)(System.currentTimeMillis() - initTime) / 1000f;

			//Check goals every 24 frames
			goalCheckCounter--;
			if (goalCheckCounter < 0){
				for(int i = 0; i < ents.size(); i++){
					if(ents.get(i) instanceof Unit){
						Unit u = (Unit) ents.get(i);
						u.updateAI(this);
					} //if units
					else if(ents.get(i) instanceof Tower){
						Tower t = (Tower) ents.get(i);
						float distanceSqr = t.getLoc().distanceSquared(player.getLoc());
						Vector3D dir = player.getLoc().difference(t.getLoc()).normalized();

						t.setRot( new Vector3D(0, (float)(Math.atan2(dir.getX(), dir.getZ())), PI));

						if(distanceSqr < TOWER_ATTACK_RADIUS * TOWER_ATTACK_RADIUS)
						{
							t.addBeam(this);
						}
					} 
				} 

				boolean allCompleted = true;
				boolean failed = false;

				for (Objective g : goals){
					g.check(this, elapsedTime);

					if (!g.isCompleted())
						allCompleted = false;
					if (g.isFailed())
						failed = true;
				}

				if (failed){
					changeState(GAME_OVER);
				} else if(allCompleted){
					changeState(VICTORY);
				}

				goalCheckCounter = GOAL_CHECK_FREQUENCY;
			}

			//Lighting setup
			ambientLight(74, 91, 133);
			directionalLight(255,244,214,-1,.5f,-0.5f);


			//Move player
			Vector3D shipRot = player.getRot();
			Vector3D shipVel = player.getVelocity();

			float frameDelta = 1.0f / frameRate;
			if(pA){
				shipRot.setY(shipRot.getY() + PI * PLAYER_TURN_SPEED * frameDelta);
			}
			if(pD){
				shipRot.setY(shipRot.getY() - PI * PLAYER_TURN_SPEED * frameDelta);

			}
			if(pW){
				shipVel.setX(sin(shipRot.getY()) * PLAYER_SPEED_FORWARD);
				shipVel.setZ(cos(shipRot.getY()) * PLAYER_SPEED_FORWARD);
				player.move(frameRate);
			}
			else if(pS){
				shipVel.setX(-sin(shipRot.getY()) * PLAYER_SPEED_BACKWARD);
				shipVel.setZ(-cos(shipRot.getY()) * PLAYER_SPEED_BACKWARD);
				player.move(frameRate);
			}
			else
			{
				shipVel.setX(0);
				shipVel.setZ(0);
				player.move(frameRate);
			}
			translate(-player.getLoc().getX(), 0, -player.getLoc().getZ());

			//Auto attack setup
			player.addProjectile(this);

			//Ability activation

			//Draw all objects
			player.draw(this);

			for(int i = 0; i < ents.size(); i++){
				Entity current = ents.get(i);
				if(current.isAlive()){
					current.move(frameRate);
					current.draw(this);
				} else {
					ents.remove(i);
				}
			}

			//AI code

			if(!player.isAlive()){
				changeState(GAME_OVER);
			}
			break;
		case GAME_OVER:
			onDeath.draw();
			break;
		case VICTORY:
			victoryMenu.draw();
			break;
		}
	}


	/**
	 * Creates a fullscreen application, and sets up OpenGL rendering.
	 */
	public void settings(){
		fullScreen(P3D);
	}


	public Player getPlayer(){
		return player;
	}

	public ArrayList<Objective> getObjectives(){
		return goals;
	}

	/**
	 * Initializes player models, sets Camera XYZ coordinates, loads models, defines preset entities to spawn in.
	 */
	public void setup(){
		//Init arraylists
		ents = new ArrayList<Entity>();
		goals = new ArrayList<Objective>();	
		PShape laser = loadShape("models/Laser.obj");
		PShape eLaser = loadShape("models/EnemyLaser.obj");
		laser.scale(3, 3, 7.5f);
		eLaser.scale(3, 3, 7.5f);

		//Create player
		Vector3D loc = new Vector3D(0, 0, 0);
		Vector3D rot = new Vector3D(0, 0, PI);


		player = new Player(loc, rot, loadShape("models/ISD1.obj"), playerMaxHealth, playerMaxShields, laser);
		updateDifficulty();
		player.getModel().scale(5);

		//Create enemy prefab
		Unit nebulonB = new Unit(new Vector3D(),
				new Vector3D(),
				loadShape("models/NebulonB.obj"), ENEMY_HEALTH, 0, eLaser);
		nebulonB.getModel().scale(10);
		nebulonB.setTarget(player);
		nebulonB.setShootSound(new Audio("sound/weapon_lightlaser_1.wav"));
		nebulonB.getShootSound().setVolume(-10);
		nebulonB.setTurretCooldown(0.6f);

		//Create boss prefab
		Unit boss = new Unit(new Vector3D(),
				new Vector3D(),
				loadShape("models/ISD1_rebel.obj"), BOSS_HEALTH, 0, eLaser);
		boss.getModel().scale(5);
		boss.setTarget(player);
		Audio deathSound = new Audio("sound/death_isd1_1.WAV");
		boss.setDeathSound(deathSound);
		boss.setTurretCooldown(0.3f);

		//Create tower prefab
		Tower tower = new Tower(new Vector3D(),
				new Vector3D(),
				loadShape("models/RailgunTower.obj"), TOWER_HEALTH, 0);
		tower.getModel().scale(10);
		tower.setTarget(player);
		tower.setShootSound(new Audio("sound/weapon_tower_5.wav"));
		tower.getShootSound().setVolume(-6);
		tower.setTurretCooldown(0.9f);

		//Setup HUD
		background = loadImage("ui/Background_level.jpg");
		hud = new HUD(this, background);

		//Setup camera
		cameraPos.setZ(-240); //-170
		cameraPos.setY(-30);
		cameraRot.setX(-2 * PI / 9);
		cameraRot.setY(PI);
		setupCamera();

		//Create menus
		startMenu = new StartMenu(this);
		onDeath = new OnDeathMenu(this);
		instructions = new InstructionMenu(this);
		victoryMenu = new VictoryMenu(this);


		//Projectile prefabs

		//Hint model
		PShape hintModel = loadShape("models/Hint.obj");
		hintModel.scale(20);

		//Asteroid prefabs
		Debris bigAsteroid = new Debris(new Vector3D(), new Vector3D(),
				loadShape("models/Asteroid_big.obj"), new Vector3D(.1f, .2f, .3f));
		bigAsteroid.getModel().scale(10);

		Debris medAsteroid = new Debris(new Vector3D(), new Vector3D(),
				loadShape("models/Asteroid_medium.obj"), new Vector3D(.2f, .4f, .6f));
		medAsteroid.getModel().scale(10);

		Debris smallAsteroid = new Debris(new Vector3D(), new Vector3D(),
				loadShape("models/Asteroid_small.obj"), new Vector3D(.4f, .8f, 1.2f));
		smallAsteroid.getModel().scale(10);

		//Create level
		createLevel(nebulonB, tower, boss, bigAsteroid, medAsteroid, smallAsteroid, hintModel);

		//Start music
		startMenuMusic.loop();

	}

	private void createLevel(Unit nebulonB, Tower railTower, Entity boss, Entity big, Entity medium, Entity small, PShape hintModel){
		Level level = new Level(this, hintModel);
		level.firstLocation();
		level.firstEnemyGroup(nebulonB);
		level.secondLocation();
		level.secondEnemyGroup(nebulonB);
		level.thirdLocation();
		level.bossGroup(boss);

		level.obstacleTowers(railTower);

		level.assortedEnemies(nebulonB);
		level.assortedTowers(railTower);

		level.decoration(big, medium, small);
	}


	/* VERY IMPORTANT
	 * CAMERA VALUES ARE SET AT THE BEGINNING TO -WIDTH/2, -HEIGHT/2, AND -(HEIGHT/2) / TAN(30).
	 * THIS FUNCTION UNDOES THAT SYSTEM, SETS THE CAMERA TO POSITION 0, 0, 0.
	 * ROTATES 40 DEGREES UP ON THE X-AXIS.
	 */
	private void setupCamera(){
		beginCamera();
		camera();
		translate(width / 2 + cameraPos.getX(), height / 2 - 30, (height/2.0f) / tan(PI*30.0f / 180.0f) + cameraPos.getZ());
		rotateX(-2 * PI / 9);
		rotateY(PI);
		endCamera();
	}

	public void keyPressed(){
		if(state != IN_GAME)
			return;
		if(keyCode == KeyEvent.VK_W){
			pW = true;
		}
		if (keyCode == KeyEvent.VK_S){
			pS = true;
		}
		if(keyCode == KeyEvent.VK_A){
			pA = true;
		}
		if(keyCode == KeyEvent.VK_D){
			pD = true;
		}		
		if(keyCode == KeyEvent.VK_Z){
			player.ability1(this);
		}
		if(keyCode == KeyEvent.VK_X){
			player.ability2(this);
		}
		if(keyCode == KeyEvent.VK_C){
			player.ability3(this);
		}


	}


	public void keyReleased(){
		if(keyCode == KeyEvent.VK_W){
			pW = false;
		} else if (keyCode == KeyEvent.VK_S){
			pS = false;
		} else if(keyCode == KeyEvent.VK_A){
			pA = false;
		} else if(keyCode == KeyEvent.VK_D){
			pD = false;
		}
	}

	public void changeState(int newState)
	{
		if (newState == IN_GAME){
			state = IN_GAME;
			startMenuMusic.reset();
			gameMusic.loop();
			gameStartSound.play();
			initTime = System.currentTimeMillis();
		}
		else if (newState == IN_START_MENU){
			state = IN_START_MENU;
			setup();
			gameMusic.reset();
			deathMenuMusic.reset();
			//startMenuMusic.loop(); already set in setup()
		}
		else if (newState == GAME_OVER){
			for(int i = 0; i < ents.size(); i++){
				Entity e = ents.get(i);
				if(e instanceof Ability)
					((Ability) e).destroy(this);
			}
			state = GAME_OVER;
			camera();
			deathMenuMusic.reset();
			deathMenuMusic.play();
			gameMusic.reset();
		}
		else if (newState == INSTRUCTIONS){
			state = INSTRUCTIONS;
			camera();
			gameMusic.reset();
		}
		else if (newState == VICTORY){
			state = VICTORY;
			camera();
			//gameMusic.reset();
		}
	}

	public void updateDifficulty(){
		if (difficulty > HARD)
			difficulty = EASY;

		if (difficulty == EASY)	{
			playerMaxHealth = PLAYER_MAX_HEALTH_E;
			playerMaxShields = PLAYER_MAX_SHIELDS_E;
		} else if (difficulty == MEDIUM)	{
			playerMaxHealth = PLAYER_MAX_HEALTH_M;
			playerMaxShields = PLAYER_MAX_SHIELDS_M;		
		} else if (difficulty == HARD)	{
			playerMaxHealth = PLAYER_MAX_HEALTH_H;
			playerMaxShields = PLAYER_MAX_SHIELDS_H;		
		}

		player.setHealth(playerMaxHealth);
		player.setMaxHealth(playerMaxHealth);
		player.setShields(playerMaxShields);
		player.setMaxShields(playerMaxShields);
	}

	public void mousePressed(){
		if(state == IN_START_MENU){
			startMenu.click(mouseX, mouseY);

		}
		else if(state == GAME_OVER){
			onDeath.click(mouseX, mouseY);
		}
		else if(state == INSTRUCTIONS){
			instructions.click(mouseX, mouseY);
		}
		else if(state == VICTORY){
			victoryMenu.click(mouseX, mouseY);
		} else {
			//Target picking
			for(int i = 0; i < ents.size(); i++){
				Entity curr = ents.get(i);
				if(curr instanceof Unit || curr instanceof Tower){
					float x = curr.getLoc().getX(); float y = curr.getLoc().getY(); float z = curr.getLoc().getZ();
					float scX = screenX(x, y, z); float scY = screenY(x, y, z);
					float dis = (float)Math.sqrt((scX - mouseX) * (scX - mouseX) + (scY - mouseY) * (scY - mouseY));
					if(dis < 65){
						if (curr.getLoc().distanceSquared(player.getLoc()) < PLAYER_ATTACK_RADIUS * PLAYER_ATTACK_RADIUS){
							player.setTarget(curr);
						}
						else {
							uiErrorSound.reset();
							uiErrorSound.play();
						}
						break;
					} 
				} 
			} 
		}
	}

	public ArrayList<Entity> getEntities(){
		return ents;
	}

	public float getElapsedTime() {
		return elapsedTime;
	}


	public int getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}


}
