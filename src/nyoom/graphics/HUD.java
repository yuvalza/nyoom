package nyoom.graphics;


import java.text.DecimalFormat;
import java.util.ArrayList;

import nyoom.entities.Entity;
import nyoom.entities.Player;
import nyoom.entities.Unit;
import nyoom.objectives.Objective;
import processing.core.PFont;
import processing.core.PImage;

public class HUD {
	private Canvas c;
	private float width;
	private float height;

	private float healthX;
	private float healthY;
	private float healthWidth;
	private float healthHeight;
	
	private float enemyHealthY;
	
	private float abilityY;
	private float abilityX;
	private float abilitySize;
	
	private float textX;
	
	private float objectiveX;
	private float objectiveY;
	private float objectiveWidth;
	private float objectiveHeight;
	
	private float padding;
	private float textPadding;
	private float textHeight;
	
	
	private PFont titleFont;
	private PFont textFont;

	private PImage background;
	
	private PImage imageShields;
	private PImage imageHealth;
	
	private PImage imageZ;
	private PImage imageX;
	private PImage imageC;
	
	DecimalFormat df = new DecimalFormat( "#,###,###,##0.0" );
	
	/**
	 * Initializes a HUD, to be drawn on Canvas c with a given background.
	 * @param c - The canvas for the HUD to be drawn on.
	 * @param background - The background image to be used.
	 */
	public HUD(Canvas c, PImage background){
		this.c = c;
		this.background = background;
		width = c.width;
		height = c.height;
		
		padding = height * 0.01f;
		textPadding = height * 0.016f;

		healthWidth = width * 0.4f;
		healthHeight = height * 0.02f;
		healthX = width * 0.3f;
		healthY = height - healthHeight * 2 - padding * 2;
		enemyHealthY = padding;
		
		abilitySize = height * 0.1f;
		abilityY = height - padding;
		abilityX = width - abilitySize - padding;
		
		textX = width * 0.5f;
		textHeight = 22f;
		
		objectiveWidth = width * 0.3f - padding * 2;
		objectiveHeight = height * 0.4f;
		objectiveX = padding;
		objectiveY = height - objectiveHeight - padding;
				
		titleFont = c.createFont("ui/Monoton-Regular.ttf", 22);
		textFont = c.createFont("ui/Hind-Medium.ttf", 22);
		
		imageShields = c.loadImage("ui/Bar_shields.png");
		imageHealth = c.loadImage("ui/Bar_health.png");
		
		imageZ = c.loadImage("ui/Abilities_seismic.png");
		imageX = c.loadImage("ui/Abilities_laser.png");
		imageC = c.loadImage("ui/Abilities_tractor.png");
	}
	
	/**
	 * Draws the HUD on the Canvas given in the constructor.
	 */
	public void draw(){
		Player p = c.getPlayer();
		Entity currTarget = c.getPlayer().getTarget();
		float scaledHP = p.getHealth() / p.getMaxHealth() * healthWidth;
		float scaledS = p.getShields() / p.getMaxShields() * healthWidth;
		float scaledZ = (p.getTimer(0) / p.getCooldown(0));
		float scaledX = (p.getTimer(1) / p.getCooldown(1));
		float scaledC = (p.getTimer(2) / p.getCooldown(2));
		c.hint(c.DISABLE_DEPTH_TEST);
		c.camera();
		c.pushStyle();
		
		//Background
		c.image(background, 0, 0, width, height);

		//Font style
		c.textFont(textFont);
		c.textLeading(textFont.getSize() + 2);
		
		//Draw settings for bars
		c.strokeWeight(4);
		c.stroke(255, 255, 255);
		c.fill(0, 0, 0, 191);
		
		//Sheilds
		c.rect(healthX, healthY, healthWidth, healthHeight);
		//c.fill(0, 126, 255);
		c.image(imageShields, healthX, healthY, scaledS, healthHeight);
		
		//Health
		c.rect(healthX, healthY + healthHeight + padding, healthWidth, healthHeight);
		//c.fill(255, 70, 0);
		c.image(imageHealth, healthX, healthY + healthHeight + padding, scaledHP, healthHeight);
		
		//Enemy Health
		if(currTarget != null && currTarget.isAlive()){
			float scaledEnemyHealth = currTarget.getHealth() / currTarget.getMaxHealth() * healthWidth;
			//System.out.print(scaledEnemyHealth);

			c.rect(healthX, enemyHealthY, healthWidth, healthHeight);
			//c.fill(255, 70, 0);
			c.image(imageHealth, healthX, enemyHealthY, scaledEnemyHealth, healthHeight);
		}

		//TODO: MAKE NEW ICON FOR Z!!!!
		//Ability Z
		c.image(imageZ, abilityX, abilityY - (abilitySize + padding) * 3, abilitySize, abilitySize);
		c.noStroke();
		c.fill(0, 0, 0, 127);
		c.rect(abilityX, abilityY - (abilitySize + padding) * 3, abilitySize, abilitySize * scaledZ);
		c.noFill();
		c.stroke(255, 255, 255);
		c.rect(abilityX, abilityY - (abilitySize + padding) * 3, abilitySize, abilitySize);
		c.fill(255, 255, 255);
		c.text('Z', abilityX - padding * 2, abilityY - (abilitySize + padding) * 3 + padding * 2);
		
		//Ability X
		c.image(imageX, abilityX, abilityY - (abilitySize + padding) * 2, abilitySize, abilitySize);
		c.noStroke();
		c.fill(0, 0, 0, 127);
		c.rect(abilityX, abilityY - (abilitySize + padding) * 2, abilitySize, abilitySize * scaledX);
		c.noFill();
		c.stroke(255, 255, 255);
		c.rect(abilityX, abilityY - (abilitySize + padding) * 2, abilitySize, abilitySize);
		c.fill(255, 255, 255);
		c.text('X', abilityX - padding * 2, abilityY - (abilitySize + padding) * 2 + padding * 2);
		
		//Ability C
		c.image(imageC, abilityX, abilityY - (abilitySize + padding), abilitySize, abilitySize);
		c.noStroke();
		c.fill(0, 0, 0, 127);
		c.rect(abilityX, abilityY - (abilitySize + padding), abilitySize, abilitySize * scaledC);
		c.noFill();
		c.stroke(255, 255, 255);
		c.rect(abilityX, abilityY - (abilitySize + padding), abilitySize, abilitySize);
		c.fill(255, 255, 255);
		c.text('C', abilityX - padding * 2, abilityY - (abilitySize + padding) + padding * 2);
		
		//Objectives
		c.fill(0, 0, 0, 191);
		c.noStroke();
		c.rect(objectiveX, objectiveY, objectiveWidth, objectiveHeight);
		c.fill(255,255,255);
		c.textAlign(c.CENTER);
		c.text("OBJECTIVE LOG", objectiveX + objectiveWidth / 2, objectiveY - padding);
		
		ArrayList<Objective> objectives = c.getObjectives();
		
		c.textAlign(c.LEFT);
		for (int i = 0; i < objectives.size(); i++)
		{
			Objective o = objectives.get(i);
			String time = "";
			if (!o.isCompleted())
				time = " (" + df.format(o.getTimeLimit() - c.getElapsedTime()) + "s) ";
			if (i == 0 || objectives.get(i - 1).isCompleted())
				c.text((i + 1) + ". " + o.getMessage() + time, objectiveX + padding, height - padding *2f - textHeight * 2.5f - textHeight * 2.5f * i,
					objectiveWidth - padding * 2, objectiveHeight - padding * 2);
		}
		
		//Bar Text
		c.fill(255, 255, 255, 127);
		c.rectMode(c.CENTER);
		c.textAlign(c.CENTER);
		
		//Shields
		c.text((int)p.getShields() + " / " + (int)p.getMaxShields(),
				textX, healthY + textPadding);
		//Health
		c.text((int)p.getHealth() + " / " + (int)p.getMaxHealth(),
				textX, healthY + padding + healthHeight + textPadding);
		//Enemy Health
		if(currTarget != null && currTarget.isAlive()){
			c.text((int)currTarget.getHealth() + " / " + (int)currTarget.getMaxHealth(),
					textX, enemyHealthY + textPadding);
		}
				
		c.popStyle();
		c.hint(c.ENABLE_DEPTH_TEST);

	}
}
