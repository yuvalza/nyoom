package nyoom.graphics;

public class Vector3D {
	private float x;
	private float y;
	private float z;
	
	/**
	 * Creates a new Vector3D with the given x, y, z values.
	 * @param x - x-value.
	 * @param y - y-value.
	 * @param z - z-value.
	 */
	public Vector3D(float x, float y, float z){
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}
	
	/**
	 * Copies the attributes of another Vector3D.
	 * @param v - Vector to be copied from.
	 */
	public Vector3D(Vector3D v){
		this.setX(v.x);
		this.setY(v.y);
		this.setZ(v.z);
	}

	/**
	 * Initializes a Vector3D with x = 0, y = 0, z = 0.
	 */
	public Vector3D(){
		x = 0f;
		y = 0f;
		z = 0f;
	}
	
	/**
	 * Calculates 3-dimensional distance to another vector.
	 * @param other The vector to calculate distance to.
	 * @return 3D distance to Vector3D other.
	 */
	public float distanceOf(Vector3D other){
		float dX = x - other.x;
		float dY = y - other.y;
		float dZ = z - other.z;
		float sum = dX * dX + dY * dY + dZ * dZ;
		return (float)Math.sqrt(sum);
	}
	
	public float distanceSquared(Vector3D other){
		float dX = x - other.x;
		float dY = y - other.y;
		float dZ = z - other.z;
		return dX * dX + dY * dY + dZ * dZ;

	}
	
	/***
	 * Returns the scalar product of this vector and a scale factor
	 * @param scale - scale factor
	 * @return a copy of this vector with the value of each axis multiplied by scale
	 */
	public Vector3D scalarProduct(float scale){
		return new Vector3D(x * scale, y * scale, z * scale);
	}
	
	/**
	 * Normalizes the vector.
	 * @return A normalized Vector3D object, with magnitude = 1.
	 */
	public Vector3D normalized(){
		float mag = getMagnitude();
		return new Vector3D(x / mag, y / mag, z / mag);
	}
	/**
	 * Calculates the vector's magnitude.
	 * @return The vector's magnitude.
	 */
	public float getMagnitude(){
		float sum = x * x + y * y + z * z;
		return (float)Math.sqrt(sum);
	}

	/**
	 * Subtracts two vectors.
	 * @param toSubtract a vector to subtract from this vector.
	 * @return The result of the subtraction of this - toSubtract.
	 */
	public Vector3D difference(Vector3D toSubtract){
		return new Vector3D(x - toSubtract.x, y - toSubtract.y, z - toSubtract.z);
	}
	
	
	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public String toString() {
		return "Vector3D [x=" + x + ", y=" + y + ", z=" + z + "]";
	}
}
