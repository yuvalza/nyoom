package nyoom.abilities;

import java.awt.Color;
import java.util.ArrayList;

import nyoom.entities.Entity;
import nyoom.entities.Tower;
import nyoom.entities.Unit;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;

public class Beam extends Ability {
	private Entity target;
	private int totalDamage = 500;
	private long initTime;
	private int elapsedTime;
	
	private Color innerCol;
	private Color outerCol;

	private static final int segments = 10; //Resolution of beam
	
	private float taperFactor = 0; //0 is no taper, 1 is strongest taper. How much the beam grows as it gets closer to the target
	private float wavyFactor = 5; //How much the sin wave varies over the course of the beam. - = towards player, + = towards target
	
	private float outerThickness = 8;
	private float innerThickness = 3;
	
	private float randomAmount = 1;
	
	private float waveAmplitude = 2;
	private float waveRate = 5; //Oscillations per second in sin wave
	
	private static final float innerOffset = .1f; //To prevent overlap of inner and outer lines
	
	private boolean useAdditive = false;
	
	/***
	 * Creates a new AoE ability entity
	 * @param loc - location
	 * @param e - all entities which can be damaged
	 * @param duration - how long this exists
	 * @param audio - Sound effect file path.
	 * @param outer - Color of the outer side of the beam.
	 * @param target - Target for the beam.
	 * @param inner - Color of the inner side of the beam.
	 * @param dmg - Total damage done by the beam.
	 */
	public Beam(Vector3D loc, Entity target, ArrayList<Entity> e, float duration, int dmg, Color inner, Color outer, String audio) {
		super(loc, new Vector3D(), duration, audio);
		this.target = target;
		initTime = System.currentTimeMillis();
		innerCol = inner;
		outerCol = outer;
		totalDamage = dmg;
	}
	
	/**
	 * Updates the ability's timer, and damages enemy targeted.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		super.move(fps);
		float deltaFrame = 1f / fps;
		target.takeHit(((totalDamage / super.getDuration()) * deltaFrame));
	}
	
	/**
	 * Draws this ability on the provided Canvas.
	 * @param renderer - Canvas to draw the ability on.
	 */
	public void draw(Canvas renderer) {
		if (!target.isAlive())
		{
			destroy(renderer);
			return;
		}
		super.draw(renderer);
		Vector3D loc = new Vector3D(getLoc());
		loc.setY(loc.getY() + 3f + (renderer.getEntities().size() % 2));
		
		elapsedTime = (int)(System.currentTimeMillis() - initTime);
		
		
		Vector3D loc2 = target.getLoc();

		
		renderer.noFill();
		if (useAdditive)
			renderer.blendMode(Canvas.ADD);
		else
			renderer.blendMode(Canvas.NORMAL);
		
		renderer.beginShape();
		renderer.stroke(outerCol.getRed(), outerCol.getGreen(), outerCol.getBlue(), outerCol.getAlpha());
		
		for (int i = 0; i <= segments; i++)
		{
			float progress = i * 1.0f/segments;
			float wave = (float)Math.sin((elapsedTime / 1000f) * (waveRate * Math.PI) - (i * wavyFactor)) * waveAmplitude + outerThickness;
			renderer.strokeWeight((wave * (1 - taperFactor)) + (wave * progress * taperFactor));
			if (i == 0)
				renderer.strokeWeight(0);

			float randy = randomAmount * (float)(Math.random() - 0.5);
			if (i == 0 || i == segments)
				randy = 0;
	
			renderer.vertex((loc.getX() * (1 - progress) + loc2.getX() * (progress)) + randy,
					(loc.getY() * (1 - progress) + loc2.getY() * (progress)) + randy,
					(loc.getZ() * (1 - progress) + loc2.getZ() * (progress)) + randy);	
		}
		renderer.endShape();
		
		renderer.beginShape();
		renderer.stroke(innerCol.getRed(), innerCol.getGreen(), innerCol.getBlue(), innerCol.getAlpha());
		
		for (int i = 0; i <= segments; i++)
		{

			float progress = i * 1.0f/segments;
			float wave = (float)Math.sin((elapsedTime / 1000f) * (waveRate * Math.PI) - (i * wavyFactor)) * waveAmplitude + innerThickness;
			renderer.strokeWeight((wave * (1 - taperFactor)) + (wave * progress * taperFactor));
			if (i == 0)
				renderer.strokeWeight(0);
			
			float randy = randomAmount * (float)(Math.random() - 0.5);
			if (i == 0 || i == segments)
				randy = 0;
			
			renderer.vertex(0 + (loc.getX() * (1 - progress) + loc2.getX() * (progress)) + randy,
					-innerOffset + (loc.getY() * (1 - progress) + loc2.getY() * (progress)) + randy,
					0 + (loc.getZ() * (1 - progress) + loc2.getZ() * (progress)) + randy);	
		}
		renderer.endShape();
		renderer.blendMode(Canvas.NORMAL);
	}
	
	

	public float getTaperFactor() {
		return taperFactor;
	}

	public void setTaperFactor(float taperFactor) {
		this.taperFactor = taperFactor;
	}

	public float getWavyFactor() {
		return wavyFactor;
	}

	public void setWavyFactor(float wavyFactor) {
		this.wavyFactor = wavyFactor;
	}

	public float getOuterThickness() {
		return outerThickness;
	}

	public void setOuterThickness(float outerThickness) {
		this.outerThickness = outerThickness;
	}

	public float getInnerThickness() {
		return innerThickness;
	}

	public void setInnerThickness(float innerThickness) {
		this.innerThickness = innerThickness;
	}

	public float getRandomAmount() {
		return randomAmount;
	}

	public void setRandomAmount(float randomAmount) {
		this.randomAmount = randomAmount;
	}

	public float getWaveAmplitude() {
		return waveAmplitude;
	}

	public void setWaveAmplitude(float waveAmplitude) {
		this.waveAmplitude = waveAmplitude;
	}

	public float getWaveRate() {
		return waveRate;
	}

	public void setWaveRate(float waveRate) {
		this.waveRate = waveRate;
	}

	public boolean isUseAdditive() {
		return useAdditive;
	}

	public void setUseAdditive(boolean useAdditive) {
		this.useAdditive = useAdditive;
	}
}
