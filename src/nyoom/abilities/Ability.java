package nyoom.abilities;

import nyoom.entities.Entity;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import nyoom.sound.Audio;
import processing.core.PShape;

public class Ability extends Entity {
	private float duration;
	private float timer;
	private Audio ambient;
	
	/***
	 * Creates a new entity Ability representation
	 * @param loc - location
	 * @param rot - rotation
	 * @param fileName - name of sound file to loop while this exists
	 */
	public Ability(Vector3D loc, Vector3D rot, String fileName){
		super(loc, rot, new PShape());	
		duration = 1.0f;
		ambient = new Audio(fileName);
		ambient.loop();
		timer = duration;
	}
	
	/***
	 * Creates a new entity Ability representation
	 * @param loc - location
	 * @param rot - rotation
	 * @param duration - time to stay alive
	 * @param fileName - name of sound file to loop while this exists
	 */
	public Ability(Vector3D loc, Vector3D rot, float duration, String fileName){
		super(loc, rot, new PShape());	
		this.duration = duration;
		ambient = new Audio(fileName);
		ambient.loop();
		timer = duration;
	}
	
	
	/**
	 * Updates the Ability timer.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps)
	{
		timer -= 1/fps;
	}
	
	
	/**
	 * Draws this ability's model.
	 * @param renderer - The canvas to be drawn on.
	 */
	public void draw(Canvas renderer)
	{
		if (timer < 0)
		{
			destroy(renderer);
		}
	}
	
	/**
	 * Removes this entity from the game and stops any sound from it.
	 * @param renderer - canvas to remove from
	 */
	public void destroy(Canvas renderer)
	{
		renderer.getEntities().remove(this);
		ambient.reset();
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public Audio getAmbient() {
		return ambient;
	}

	public void setAmbient(Audio ambient) {
		this.ambient = ambient;
	}
}
