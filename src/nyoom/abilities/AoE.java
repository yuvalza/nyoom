package nyoom.abilities;

import java.util.ArrayList;

import nyoom.entities.Entity;
import nyoom.entities.Tower;
import nyoom.entities.Unit;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;

public class AoE extends Ability {
	private static final float SIZE = 100;
	private static final int VERTEX_COUNT = 6; //How many sides to the procedural AoE graphic?
	private ArrayList<Entity> entities;
	private float damagePerSecond;
	
	/***
	 * Creates a new AoE ability entity
	 * @param loc - location
	 * @param rot - rotation
	 * @param e - all entities which can be damaged
	 * @param duration - how long this exists
	 * @param dps - Damage dealt per second.
	 */
	public AoE(Vector3D loc, Vector3D rot, ArrayList<Entity> e, float duration, float dps) {
		super(loc, rot, duration, "sound/ambient_aoe_1.wav");
		entities = e;
		damagePerSecond = dps;
	}
	
	/**
	 * Updates the ability's timer, and damages any enemies in its radius.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		super.move(fps);
		float deltaFrame = 1f / fps;
		for(int i = 0; i < entities.size(); i++){
			Entity curr = entities.get(i);
			if(curr instanceof Unit || curr instanceof Tower){ //Player is not in this arraylist, so not affected
				float distSquared = (curr.getLoc().getX() - getLoc().getX()) * (curr.getLoc().getX() - getLoc().getX())
						+ (curr.getLoc().getZ() - getLoc().getZ()) * (curr.getLoc().getZ() - getLoc().getZ());
				if(distSquared < SIZE * SIZE){  //Avoids costly sqrRoot calculations every frame
					curr.takeHit((damagePerSecond * deltaFrame));
				}
			}
		}
	}
	
	
	/**
	 * Draws this ability on the provided Canvas.
	 * @param renderer - Canvas to draw the ability on.
	 */
	public void draw(Canvas renderer) {
		super.draw(renderer);
		Vector3D loc = getLoc();
		
		renderer.beginShape();
		renderer.fill(255, 80, 0, 60);
		renderer.stroke(255, 150, 20);
		renderer.strokeWeight(2);
		
		
		//Generate circle shape
		for (int i = 0; i < VERTEX_COUNT; i++) {
			float randy = 0.015f * (float)(Math.random() - 0.5) + 1.0f;
			float wave = (float)Math.sin(System.nanoTime() * 0.05f) * 0.1f + 1.0f;
			wave = 1;
			renderer.vertex(loc.getX() + (float)(Math.cos(Math.PI * 2  * i / VERTEX_COUNT) * SIZE * randy * wave), 8.0f, loc.getZ() + (float)(Math.sin(Math.PI * 2 * i / VERTEX_COUNT) * SIZE * randy * wave));
		}
		
		renderer.endShape(renderer.CLOSE);
	}
}
