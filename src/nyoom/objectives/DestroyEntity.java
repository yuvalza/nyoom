package nyoom.objectives;

import java.util.ArrayList;

import nyoom.entities.Entity;
import nyoom.graphics.Canvas;

public class DestroyEntity extends Objective {
	private ArrayList<Entity> toDestroy;
	
	/***
	 * Creates a new DestroyEntity objective
	 * @param description - what the objective is
	 * @param completeMessage - congratulation message
	 * @param targets - what entities have to be destroyed
	 * @param time - Time limit for the objective to be completed.
	 */
	public DestroyEntity(String description, String completeMessage, ArrayList<Entity> targets, float time) {
		super(description, completeMessage, time);
		toDestroy = targets;
	}
	
	/**
	 * Checks whether all Entities within the given ArrayList have been destroyed.
	 * @param drawer - Canvas on which the objective is to be met.
	 * @param time - The current time in milliseconds since objective has started.
	 */
	public void check(Canvas drawer, float time) {
		super.check(drawer, time);
		
		boolean contains = false;
		
		for(Entity ent : toDestroy)
		{
			if (drawer.getEntities().contains(ent))
				contains = true;
		}
		
		if (!contains){
			super.setCompleted(true);
		}
	}
}
