package nyoom.objectives;

import nyoom.graphics.Canvas;

public class Objective {
	private String desc;
	private String complete;
	private boolean isCompleted;
	private boolean isFailed;
	private float timeLimit;
	
	/***
	 * Creates a new objective
	 * @param description - what the objective is
	 * @param completeMessage - congratulation message
	 * @param time - Time limit for the objective to be completed.
	 */
	public Objective(String description, String completeMessage, float time) {
		setDesc(description);
		setComplete(completeMessage);
		setCompleted(false);
		timeLimit = time;
	}
	
	/**
	 * Checks if the conditions for the objective are fulfilled
	 * @param drawer - canvas to check against
	 * @param time - time in milliseconds since the objective has begun.
	 */
	public void check(Canvas drawer, float time) {
		if (isCompleted())
			return;
		
		if (time > timeLimit){
			setFailed(true);
		}
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getComplete() {
		return complete;
	}

	public void setComplete(String complete) {
		this.complete = complete;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	/***
	 * Returns either the objective description or the objective complete message based on the state of the objective
	 * @return either the objective description or the objective complete message
	 */
	public String getMessage()
	{
		if (isCompleted)
			return complete;
		else
			return desc;
	}

	public boolean isFailed() {
		return isFailed;
	}

	public void setFailed(boolean isFailed) {
		this.isFailed = isFailed;
	}

	public float getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(float timeLimit) {
		this.timeLimit = timeLimit;
	}
}