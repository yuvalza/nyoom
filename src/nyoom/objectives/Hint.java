package nyoom.objectives;

import nyoom.entities.Entity;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;
import processing.core.PShape;

public class Hint extends Entity {
	private static final float rotSpeed = 3f;
	private static final float offset = 30f;
	private Entity toFollow;
	private Objective relatedObj;
	
	/**
	 * Initializes a new Hint object with the given model, objective, location and rotation, and model.
	 * @param l - Location.
	 * @param r - Rotation.
	 * @param m - Model.
	 * @param obj - Objective.
	 */
	public Hint(Vector3D l, Vector3D r, PShape m, Objective obj) {
		super(new Vector3D(l.getX(), l.getY() + offset, l.getZ()), r, m);
		relatedObj = obj;
	}
	
	/**
	 * Initializes a new Hint object, which will follow the given Entity.
	 * @param toFollow - Entity to be followed around.
	 * @param r - Rotation.
	 * @param m - Model.
	 * @param obj - Objective,
	 */
	public Hint(Entity toFollow, Vector3D r, PShape m, Objective obj) {
		super(new Vector3D(), r, m);
		this.toFollow = toFollow;
		relatedObj = obj;
	}
	
	/**
	 * Updates this hint's location according to the entity it shoudl follow, and rotates it.
	 * @param fps - The current Frames Per Second of the Canvas.
	 */
	public void move(float fps){
		super.move(fps);
		float frameDelta = 1f/fps;
		Vector3D oldRot = super.getRot();
		super.setRot(new Vector3D(oldRot.getX(), oldRot.getY() + frameDelta* rotSpeed, oldRot.getZ()));
		
		if (toFollow != null) {
			Vector3D loc = toFollow.getLoc();
			super.setLoc(new Vector3D(loc.getX(), loc.getY() + offset, loc.getZ()));
		}	
	}
	
	/**
	 * Draws the hint on the given Canvas.
	 * @param s - Canvas to draw the hint on.
	 */
	public void draw(Canvas s)
	{
		//If completed, kill this hint	
		if (relatedObj.isCompleted())
		{
			s.getEntities().remove(this);
			return;
		}
		
		s.blendMode(s.ADD);
		super.draw(s);
		s.blendMode(s.NORMAL);
		Vector3D loc = super.getLoc();
		
	}
}
