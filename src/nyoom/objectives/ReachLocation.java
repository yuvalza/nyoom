package nyoom.objectives;

import java.util.ArrayList;

import nyoom.entities.Entity;
import nyoom.entities.Player;
import nyoom.graphics.Canvas;
import nyoom.graphics.Vector3D;

public class ReachLocation extends Objective {
	private Vector3D toReach;
	private float tolerance;
	
	/**
	 * Creates a new ReachLocation objective
	 * @param description - what the objective is
	 * @param completeMessage - congratulation message
	 * @param destination - where to go to
	 * @param completeMessage - Message displayed when the objective is completed.
	 * @param time - Time limit for the objective to be completed.
	 */
	public ReachLocation(String description, String completeMessage, Vector3D destination, float time) {
		super(description, completeMessage, time);
		toReach = destination;
		tolerance = 80;
	}
	/**
	 * Checks whether the player has reached the required location.
	 * @param drawer - Canvas on which the objective is to be met.
	 * @param time - The current time in milliseconds since objective has started.
	*/
	public void check(Canvas drawer, float time) {
		super.check(drawer, time);
		Player player = drawer.getPlayer();
		if(player.getLoc().distanceSquared(toReach) < tolerance * tolerance){ //Avoids costly sqrRoot calculations every frame
			super.setCompleted(true);
		}
		
	}
}
