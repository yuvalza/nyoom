package nyoom.sound;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Audio{
	private AudioInputStream input;
	private Clip soundClip;
	private File wavFile;
	private boolean isLooping;
	private FloatControl gain;
	
	
	/**
	 * Creates a new Audio object with looping settings
	 * @param filename - audio file to read from
	 */
	public Audio(String filename){
		isLooping = false;
		try{
			soundClip = AudioSystem.getClip();
			wavFile = new File(filename);
			input = AudioSystem.getAudioInputStream(wavFile);
			soundClip.open(input);
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		gain = (FloatControl) soundClip.getControl(FloatControl.Type.MASTER_GAIN);

	}

	/**
	 * Sets this audio to loop continuously
	 */
	public void loop(){
        soundClip.loop(Clip.LOOP_CONTINUOUSLY);
        isLooping = true;
	}
	
	/**
	 * Sets the volume to the given decibel value.
	 * @param db Volume.
	 */
	public void setVolume(float db){
		gain.setValue(db);
	}
	
	public boolean isLooping(){
		return isLooping;
	}
	
	/**
	 * Start playing the audio from the current frame positon.
	 */
	public void play(){
		soundClip.start();
	}

	/***
	 * Stop playing and reset the audio from the beginning of the file.
	 * Flushes the Audio buffer.
	 */
	public void reset(){
		soundClip.stop();
		soundClip.flush();
		soundClip.setFramePosition(0);
	}


}
