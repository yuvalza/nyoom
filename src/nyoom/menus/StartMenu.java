package nyoom.menus;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import com.sun.prism.paint.Color;

import nyoom.graphics.Canvas;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

public class StartMenu extends Menu{

	private PImage background;
	private Canvas c;
	private float width;
	private float height;
	
	private PFont titleFont;
	private PFont textFont;
	
	private float buttonX;
	private float buttonY;
	private float buttonWidth;
	private float buttonHeight;
	
	private float textPadding;
	private float buttonSpacing;
	
	private float textX;
	private float titleY;
	


	/**
	 * creates three blank rectangles that will be buttons and sets the background to a picture of space
	 * @param s - the PApplet surface on which it is drawn
	 */
	public StartMenu(Canvas s){	
		this.c = s;
		background = c.loadImage("ui/Background_title.jpg");
		width = c.width;
		height = c.height;

		titleFont = c.createFont("ui/Righteous-Regular.ttf", 140);
		textFont = c.createFont("ui/Hind-Medium.ttf", 22);
		
		textPadding = height * 0.025f;
		buttonSpacing = height * 0.02f;
		
		buttonX = width * 0.35f;
		buttonY = height * 0.4f;
		buttonWidth = width * 0.3f;
		buttonHeight = height * 0.04f;
		
		textX = c.width * 0.5f;
		titleY = buttonY - titleFont.getSize() * 0.01f - buttonSpacing;
		
		rectangles = new Rectangle[4];
		for(int i = 0; i < rectangles.length; i++){
			rectangles[i] = new Rectangle((int)buttonX, (int)(buttonY + i * (buttonHeight + buttonSpacing)), (int)buttonWidth, (int)buttonHeight);
			//c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());
		}

	}

	/**
	 * Draws this menu on the given canvas.
	 */
	public void draw() {
		c.pushStyle();
		c.image(background, 0, 0, c.width, c.height);
		c.fill(63);
		c.stroke(255, 255, 255);
		c.strokeWeight(2); 
		for(int i = 0; i < rectangles.length; i++){
			Rectangle r = rectangles[i];
			//r = new Rectangle((int)buttonX, (int)(buttonY + i * (buttonSpacing)), (int)buttonWidth, (int)buttonHeight);
			c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());
		}

		//Font setup
		c.rectMode(c.CENTER);
		c.textAlign(c.CENTER);

		
		//Title text
		c.textFont(titleFont);
		c.fill(255);
		
		c.text("NYOOM", textX, titleY);
		
		//Button text
		c.textFont(textFont);
		int difficulty = c.getDifficulty();
		String difficultyName = "";
		if (difficulty == 0)
			difficultyName = "Easy";
		else if (difficulty == 1)
			difficultyName = "Normal";
		else if (difficulty == 2)
			difficultyName = "Hard";
		
		c.text("Start Game (" + difficultyName +")", textX, (float)rectangles[0].getY() + textPadding);
		c.text("Change Difficulty", textX, (float)rectangles[1].getY() + textPadding);
		c.text("Help", textX, (float)rectangles[2].getY() + textPadding);
		c.text("Quit", textX, (float)rectangles[3].getY() + textPadding);

		c.popStyle();




	}

	/**
	 * Checks for a click on one of the menu's buttons, and changes the game state accordingly.
	 * @param x - mouse X.
	 * @param y - mouse Y.
	 */
	public void click(int x, int y) {
		int r = -1;
		for(int i = 0; i < rectangles.length; i++){
			if(rectangles[i].contains(x, y))
				r = i;
		}

		if(r == 0){
			c.changeState(c.IN_GAME);
		}
		else if(r == 1){
			c.setDifficulty(c.getDifficulty() + 1);
			c.updateDifficulty();
		}
		else if(r == 2){
			c.changeState(c.INSTRUCTIONS);
		}
		else if(r == 3){
			c.exit();
		}

	}

}

