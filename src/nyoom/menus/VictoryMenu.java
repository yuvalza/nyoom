package nyoom.menus;

import java.awt.Rectangle;

import nyoom.graphics.Canvas;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

public class VictoryMenu extends Menu{

	private PImage background;
	private Canvas c;
	private float width;
	private float height;
	
	private PFont titleFont;
	private PFont textFont;
	
	private float buttonX;
	private float buttonY;
	private float buttonWidth;
	private float buttonHeight;
	
	private float textPadding;
	private float buttonSpacing;
	
	private float textX;
	private float titleY;
	


	/**
	 * creates three blank rectangles that will be buttons and sets the background to a picture of space
	 * @param s - the PApplet surface on which it is drawn
	 */
	public VictoryMenu(Canvas s){	
		this.c = s;
		background = c.loadImage("ui/Background_victory.jpg");
		width = c.width;
		height = c.height;

		titleFont = c.createFont("ui/Monoton-Regular.ttf", 80);
		textFont = c.createFont("ui/Hind-Medium.ttf", 22);
		
		textPadding = height * 0.025f;
		buttonSpacing = height * 0.06f;
		
		buttonX = width * 0.35f;
		buttonY = height * 0.4f;
		buttonWidth = width * 0.3f;
		buttonHeight = height * 0.04f;
		
		textX = c.width * 0.5f;
		titleY = buttonY - titleFont.getSize() * 0.5f;
		
		rectangles = new Rectangle[1];
		for(int i = 0; i < rectangles.length; i++){
			rectangles[i] = new Rectangle((int)buttonX, (int)(buttonY + i * (buttonSpacing)), (int)buttonWidth, (int)buttonHeight);
			//c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());
		}

	}

	/**
	 * Draws this menu on the given canvas.
	 */
	public void draw() {
		c.pushStyle();
		c.image(background, 0, 0, c.width, c.height);
		c.noStroke();
		c.fill(0, 0, 0, 191);
		c.rect(buttonX, buttonY, buttonWidth, buttonHeight * 3f);
		
		c.fill(191);
		c.stroke(0, 0, 0);
		c.strokeWeight(2); 
		for(int i = 0; i < rectangles.length; i++){
			Rectangle r = rectangles[i];
			c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());
		}

		//Font setup
		c.rectMode(c.CENTER);
		c.textAlign(c.CENTER);

		
		//Title text
		c.textFont(titleFont);
		c.fill(255);
		
		c.text("VICTORY", textX, titleY);
		
		//Button text
		c.textFont(textFont);
		c.fill(0);
		c.text("Play Again", textX, (float)rectangles[0].getY() + textPadding);
		
		int difficulty = c.getDifficulty();
		String difficultyName = "";
		if (difficulty == 0)
			difficultyName = "Easy";
		else if (difficulty == 1)
			difficultyName = "Normal";
		else if (difficulty == 2)
			difficultyName = "Hard";
		
		String bestTime = "Best developer time: ";
		if (difficulty == 0)
			bestTime += "161.6s";
		else if (difficulty == 1)
			bestTime += "173s";
		else if (difficulty == 2)
			bestTime += "177.5s";
		
		c.fill(255);
		c.text("Your time on " + difficultyName + " Difficulty was: " + c.getElapsedTime() + "s\n" + bestTime, textX, (float)(rectangles[0].getY() + textPadding + rectangles[0].getHeight()));

		c.popStyle();




	}
	
	/**
	 * Checks for a click on one of the menu's buttons, and changes the game state accordingly.
	 * @param x - mouse X.
	 * @param y - mouse Y.
	 */
	public void click(int x, int y) {
		int r = -1;
		for(int i = 0; i < rectangles.length; i++){
			if(rectangles[i].contains(x, y))
				r = i;
		}
		if(r == 0){
			c.changeState(c.IN_START_MENU);
		}

	}

}
