package nyoom.menus;

import java.awt.Rectangle;

import nyoom.graphics.Canvas;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

public class InstructionMenu extends Menu{

	private PImage background;
	private Canvas c;
	private float width;
	private float height;

	private PFont titleFont;
	private PFont subtitleFont;
	private PFont textFont;
	private PFont fineTextFont;

	private float buttonX;
	private float buttonY;
	private float buttonWidth;
	private float buttonHeight;

	private float padding;
	private float paddingSmall;
	private float textPadding;

	private float centerX;
	private float titleHeight;
	private float titleY;

	private float subtitleY;

	private float abilityY;
	private float abilityX;

	private float abilityTextWidth;

	private PImage imageZ;
	private PImage imageX;
	private PImage imageC;
	private float abilitySize;

	private static final String ABILITY_Z_TEXT = "Deploys a hexagonal proton field which deals damage over time to all enemies located inside. Lasts for 9 seconds, recharges in 28 seconds.";
	private static final String ABILITY_X_TEXT = "An enemy must be targeted to activate. Deals a massive amount of damage to the targeted enemy, though also consumes some of your shields to activate. You must have sufficient shields to use this ability. Recharges in 18 seconds.";
	private static final String ABILITY_C_TEXT = "An enemy must be targeted to activate. Completely disables the targeted enemy, freezing it in place and preventing it from attacking you. Does not affect towers. Lasts for 5 seconds, recharges in 21 seconds.";


	/**
	 * creates three blank rectangles that will be buttons and sets the background to a picture of space
	 * @param s - the PApplet surface on which it is drawn
	 */
	public InstructionMenu(Canvas s){	
		this.c = s;
		background = c.loadImage("ui/Background_help.jpg");
		width = c.width;
		height = c.height;

		titleFont = c.createFont("ui/Monoton-Regular.ttf", 80);
		subtitleFont = c.createFont("ui/Lato-Black.ttf", 40);
		textFont = c.createFont("ui/Hind-Medium.ttf", 24);
		fineTextFont = c.createFont("ui/Hind-Medium.ttf", 20);

		padding = height * 0.02f;
		paddingSmall = height * 0.01f;
		textPadding = height * 0.025f;


		buttonWidth = width * 0.3f;
		buttonHeight = height * 0.04f;
		buttonX = width * 0.35f;
		buttonY = height - buttonHeight - padding;

		centerX = c.width * 0.5f;
		titleHeight = titleFont.getSize() * 0.85f;
		titleY = titleHeight + padding;

		subtitleY = titleY + padding * 3;


		abilitySize = width * 0.1f;
		abilityY = height * 0.6f - abilitySize;
		abilityX = centerX + padding * 2;

		rectangles = new Rectangle[1];
		rectangles[0] = new Rectangle((int)buttonX, (int)(buttonY), (int)buttonWidth, (int)buttonHeight);

		imageZ = c.loadImage("ui/Abilities_seismic.png");
		imageX = c.loadImage("ui/Abilities_laser.png");
		imageC = c.loadImage("ui/Abilities_tractor.png");

		abilityTextWidth = abilitySize + padding * 4;
	}

	/**
	 * Draws this menu on the given canvas.
	 */
	public void draw() {
		c.pushStyle();
		c.image(background, 0, 0, c.width, c.height);
		c.fill(191);
		c.stroke(0, 0, 0);
		c.strokeWeight(2); 

		Rectangle r = rectangles[0];
		c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());

		//Title text
		c.textAlign(c.CENTER);
		c.rectMode(c.CORNER);
		c.textFont(titleFont);
		c.fill(255);

		c.text("HOW TO PLAY", centerX, titleY);

		//Back button text
		c.rectMode(c.CENTER);
		c.textFont(textFont);
		c.fill(0);

		c.text("Back", centerX, (float)rectangles[0].getY() + textPadding);

		//Instructions start here
		c.textAlign(c.LEFT);
		c.rectMode(c.CORNER);
		c.stroke(255);
		c.fill(255);

		c.line(centerX, titleY + padding, centerX, buttonY - padding);

		//Subtitles
		c.textFont(subtitleFont);
		c.text("Controls", padding * 2, subtitleY);
		c.text("Abilities", centerX + padding * 2, subtitleY);


		//Controls text
		c.textFont(subtitleFont);
		c.textLeading(subtitleFont.getSize() + 2);

		c.text("W - Move forward\nS- Move backward\nA - Turn left\nD - Turn right\nClick (on enemy) - Set target (the enemy must be within range to be targeted)\n"
				+ "Z - Proton Field\nX - Superlaser\nC - Tractor Beam\nEscape - Quit game",
				padding * 2, subtitleY + paddingSmall, centerX - padding * 4, height);

		//Abilities text
		c.textFont(textFont);
		c.textLeading(textFont.getSize() + 2);
		c.text("You won't complete your objectives in time without using your abilities. They vary in effect, possibly affecting an area instead of a single target, or not even dealing any direct damage, but all of them apply some kind of negative effect onto enemies.",
				centerX + padding * 2, subtitleY + paddingSmall, centerX - padding * 4, height);		


		//Abilities
		//Proton Field
		float currentOffset = 0;
		c.image(imageZ, currentOffset + abilityX, abilityY, abilitySize, abilitySize);
		c.textFont(subtitleFont);
		c.textAlign(c.CENTER);
		c.text("Proton Field", currentOffset + abilityX + abilitySize * 0.5f, abilityY - paddingSmall * 1.5f);
		c.textFont(fineTextFont);
		c.textLeading(fineTextFont.getSize() + 2);
		c.textAlign(c.LEFT);
		c.text(ABILITY_Z_TEXT, currentOffset + abilityX - padding, abilityY + abilitySize + paddingSmall, abilityTextWidth, height);
		//Superlaser
		currentOffset = abilitySize * 1.67f;
		c.image(imageX, currentOffset + abilityX, abilityY, abilitySize, abilitySize);
		c.textFont(subtitleFont);
		c.textAlign(c.CENTER);
		c.text("Superlaser", currentOffset + abilityX + abilitySize * 0.5f, abilityY - paddingSmall * 1.5f);
		c.textFont(fineTextFont);
		c.textLeading(fineTextFont.getSize() + 2);
		c.textAlign(c.LEFT);
		c.text(ABILITY_X_TEXT, currentOffset + abilityX - padding, abilityY + abilitySize + paddingSmall, abilityTextWidth, height);		
		//Tractor Beam
		currentOffset = abilitySize * 3.33f;
		c.image(imageC, currentOffset + abilityX, abilityY, abilitySize, abilitySize);
		c.textFont(subtitleFont);
		c.textAlign(c.CENTER);
		c.text("Tractor Beam", currentOffset + abilityX + abilitySize * 0.5f, abilityY - paddingSmall * 1.5f);
		c.textFont(fineTextFont);
		c.textLeading(fineTextFont.getSize() + 2);
		c.textAlign(c.LEFT);
		c.text(ABILITY_C_TEXT, currentOffset + abilityX - padding, abilityY + abilitySize + paddingSmall, abilityTextWidth, height);		


		c.popStyle();
	}

	/**
	 * Checks for a click on one of the menu's buttons, and changes the game state accordingly.
	 * @param x - mouse X.
	 * @param y - mouse Y.
	 */
	public void click(int x, int y) {
		int r = -1;
		for(int i = 0; i < rectangles.length; i++){
			if(rectangles[i].contains(x, y))
				r = i;
		}
		if(r == 0){
			c.changeState(c.IN_START_MENU);
		}
	}

}
