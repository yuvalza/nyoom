package nyoom.menus;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;

import processing.core.PApplet;
import processing.core.PImage;

public abstract class Menu {

	Rectangle[] rectangles;

	/**
	 * Draws the menu on the Canvas provided in the constructor.
	 */
	abstract void draw();
	
	/**
	 * checks if a click is within one of the rectangles, and updates Canvas state accordingly.
	 * @param x - x location of the click
	 * @param y - y location of the click
	 */
	abstract void click(int x, int y);
}
