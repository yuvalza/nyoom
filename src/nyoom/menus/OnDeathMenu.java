package nyoom.menus;

import java.awt.Rectangle;

import nyoom.graphics.Canvas;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;

public class OnDeathMenu extends Menu{

	private PImage background;
	private Canvas c;
	private float width;
	private float height;
	
	private PFont titleFont;
	private PFont textFont;
	
	private float buttonX;
	private float buttonY;
	private float buttonWidth;
	private float buttonHeight;
	
	private float textPadding;
	private float buttonSpacing;
	
	private float textX;
	private float titleY;
	


	/**
	 * creates three blank rectangles that will be buttons and sets the background to a picture of space
	 * @param s - the PApplet surface on which it is drawn
	 */
	public OnDeathMenu(Canvas s){	
		this.c = s;
		background = c.loadImage("ui/Background_death.jpg");
		width = c.width;
		height = c.height;

		titleFont = c.createFont("ui/Monoton-Regular.ttf", 80);
		textFont = c.createFont("ui/Hind-Medium.ttf", 22);
		
		textPadding = height * 0.025f;
		buttonSpacing = height * 0.06f;
		
		buttonX = width * 0.35f;
		buttonY = height * 0.4f;
		buttonWidth = width * 0.3f;
		buttonHeight = height * 0.04f;
		
		textX = c.width * 0.5f;
		titleY = buttonY - titleFont.getSize() * 0.5f;
		
		rectangles = new Rectangle[2];
		for(int i = 0; i < rectangles.length; i++){
			rectangles[i] = new Rectangle((int)buttonX, (int)(buttonY + i * (buttonSpacing)), (int)buttonWidth, (int)buttonHeight);
		}

	}

	/**
	 * Draws this menu on the given canvas.
	 */
	public void draw() {
		c.pushStyle();
		c.image(background, 0, 0, c.width, c.height);
		c.fill(191);
		c.stroke(0, 0, 0);
		c.strokeWeight(2); 
		for(int i = 0; i < rectangles.length; i++){
			Rectangle r = rectangles[i];
			//r = new Rectangle((int)buttonX, (int)(buttonY + i * (buttonSpacing)), (int)buttonWidth, (int)buttonHeight);
			c.rect((float)r.getX(), (float)r.getY(), (float)r.getWidth(), (float)r.getHeight());
		}

		//Font setup
		c.rectMode(c.CENTER);
		c.textAlign(c.CENTER);

		
		//Title text
		c.textFont(titleFont);
		c.fill(0);
		
		c.text("MISSION FAILED", textX, titleY);
		
		//Button text
		c.textFont(textFont);
		c.text("Restart", textX, (float)rectangles[0].getY() + textPadding);
		c.text("Quit", textX, (float)rectangles[1].getY() + textPadding);

		c.popStyle();




	}
	
	/**
	 * Checks for a click on one of the menu's buttons, and changes the game state accordingly.
	 * @param x - mouse X.
	 * @param y - mouse Y.
	 */
	public void click(int x, int y) {
		int r = -1;
		for(int i = 0; i < rectangles.length; i++){
			if(rectangles[i].contains(x, y))
				r = i;
		}
		if(r == -1)
			return;
		else if(r == 0){
			c.changeState(c.IN_START_MENU);
		} else {
			c.exit();
		}
	}

}
